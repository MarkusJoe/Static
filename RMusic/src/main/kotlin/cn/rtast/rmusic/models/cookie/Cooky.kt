package cn.rtast.rmusic.models.cookie

data class Cooky(
    val cookie: String,
    val platform: String
)