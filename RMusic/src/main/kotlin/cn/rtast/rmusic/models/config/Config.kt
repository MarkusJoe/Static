package cn.rtast.rmusic.models.config

data class Config(
    val platform: String,
    val url: String,
    val modifiable: Boolean
)