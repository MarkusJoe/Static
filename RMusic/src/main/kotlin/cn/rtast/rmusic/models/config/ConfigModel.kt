package cn.rtast.rmusic.models.config

data class ConfigModel(
    val configs: List<Config>
)