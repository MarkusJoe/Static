package cn.rtast.rmusic.models.song

data class SongUrlModel(
    val code: Int,
    val `data`: List<Data>
)