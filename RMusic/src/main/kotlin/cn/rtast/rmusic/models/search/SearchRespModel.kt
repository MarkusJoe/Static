package cn.rtast.rmusic.models.search

data class SearchRespModel(
    val code: Int,
    val result: Result
)