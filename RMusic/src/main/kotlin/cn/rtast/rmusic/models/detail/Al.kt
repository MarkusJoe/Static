package cn.rtast.rmusic.models.detail

data class Al(
    val id: Int,
    val name: String,
    val pic: Long,
    val picUrl: String,
    val tns: List<Any>
)