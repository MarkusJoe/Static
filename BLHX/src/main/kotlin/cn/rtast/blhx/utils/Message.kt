package cn.rtast.blhx.utils

import com.mojang.brigadier.context.CommandContext
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.text.Text

class Message {
    fun sendFeedback(ctx: CommandContext<ServerCommandSource>, msg: String, level: String = "I") {
        ctx.source.sendFeedback(Text.literal("[BLHX][$level]: $msg"), false)
    }

    fun sendMessage(ctx: CommandContext<ServerCommandSource>, msg: String, level: String = "I") {
        ctx.source.sendFeedback(Text.literal("[BLHX][$level]: $msg"), false)
    }
}