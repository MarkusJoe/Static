package cn.rtast.blhx.commands

import cn.rtast.blhx.utils.Message
import cn.rtast.blhx.websocket.NewWebSocket
import com.mojang.brigadier.CommandDispatcher
import com.mojang.brigadier.arguments.StringArgumentType.*
import com.mojang.brigadier.context.CommandContext
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.server.command.CommandManager.*

class BLHXCommand {
    private var thread: Thread? = null
    private val message = Message()

    fun register(dispatcher: CommandDispatcher<ServerCommandSource>) {
        dispatcher.register(literal("blhx")
            .then(
                literal("start")
                    .then(
                        argument("room", string())
                            .executes { start(it, getString(it, "room"));1 }
                    )
            )
            .then(
                literal("stop")
                    .executes { interrupt(it);1 }
            )
        )
    }

    private fun start(ctx: CommandContext<ServerCommandSource>, roomID: String) {
        if (thread == null) {
            message.sendFeedback(ctx, "正在启动中...")
            thread = Thread {
                NewWebSocket().start(roomID)
            }
            thread?.start()
        } else {
            message.sendFeedback(ctx, "已经运行了一个实例!")
        }
    }

    private fun interrupt(ctx: CommandContext<ServerCommandSource>) {
        try {
            if (thread != null && !thread?.isInterrupted!!) {
                thread?.interrupt()
                message.sendFeedback(ctx, "已关闭!")
            } else {
                message.sendFeedback(ctx, "当前没有运行中的实例!")
            }
        } catch (_: Exception) {
            thread = null
        }
    }
}