package cn.rtast.blhx

import cn.rtast.blhx.commands.BLHXCommand
import net.fabricmc.api.ModInitializer
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback
import net.minecraft.client.sound.SoundInstance

class Blhx : ModInitializer {
    override fun onInitialize() {
        CommandRegistrationCallback.EVENT.register { dispatcher, _, _ ->
            BLHXCommand().register(dispatcher)
        }

    }
}