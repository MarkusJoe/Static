package cn.rtast.blhx.websocket

import okhttp3.*
import java.util.concurrent.TimeUnit

class WebSocketServer {
    fun start() {
        val listener = object : WebSocketListener() {
            override fun onOpen(webSocket: WebSocket, response: Response) {
                println("WebSocket opened")
            }

            override fun onMessage(webSocket: WebSocket, text: String) {
                println("Received message: $text")
                webSocket.send("Echo: $text")
            }

            override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
                println("WebSocket closing: $code $reason")
            }

            override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
                println("WebSocket failed")
            }
        }
        val client = OkHttpClient.Builder()
            .readTimeout(10, TimeUnit.MILLISECONDS)
            .build()
        val request = Request.Builder()
            .url("ws://localhost:8080")
            .build()
        client.newWebSocket(request, listener)
        client.dispatcher.executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS)
    }
}

fun main() {
    WebSocketServer().start()
}