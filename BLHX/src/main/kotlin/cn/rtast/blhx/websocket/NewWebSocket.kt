package cn.rtast.blhx.websocket

import com.google.gson.JsonObject
import okhttp3.*
import okio.ByteString
import okio.ByteString.Companion.toByteString
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.concurrent.CountDownLatch


class NewWebSocket {
    var countDownLatch = CountDownLatch(1)

    private fun hexToByteArray(hex: String): ByteArray {
        var hexString = hex
        if (hexString.length % 2 == 1) {
            hexString = "0$hex"
        }
        val hexLength = hexString.length
        val result = ByteArray(hexLength / 2)
        var i = 0
        var j = 0
        while (i < hexLength) {
            result[j] = hexString.substring(i, i + 2).toInt(16).toByte()
            i += 2
            j++
        }
        return result
    }

    fun start(roomID: String) {
        val client: OkHttpClient = OkHttpClient.Builder().retryOnConnectionFailure(true).build()
        val request: Request = Request.Builder().url("wss://broadcastlv.chat.bilibili.com/sub").build()
        client.dispatcher.cancelAll() //清理一次
        client.newWebSocket(request, object : WebSocketListener() {
            override fun onOpen(webSocket: WebSocket, response: Response) {
                val bodyJson = JsonObject()
                bodyJson.addProperty("uid", "0")
                bodyJson.addProperty("roomid", roomID)
                bodyJson.addProperty("protover", "1")
                bodyJson.addProperty("platform", "web")
                bodyJson.addProperty("clientver", "1.5.10.1")
                bodyJson.addProperty("type", "2")
                val packetSize = Integer.toHexString(bodyJson.toString().toByteArray().size + 16)
                val head = "000000${packetSize}001000010000000700000001"
                val headHexBytesArray = hexToByteArray(head)
                val bodyHexBytesArray = bodyJson.toString().toByteArray(StandardCharsets.UTF_8)
                val result = ByteArray(headHexBytesArray.size + bodyHexBytesArray.size)
                System.arraycopy(headHexBytesArray, 0, result, 0, headHexBytesArray.size)
                System.arraycopy(bodyHexBytesArray, 0, result, headHexBytesArray.size, bodyHexBytesArray.size)
                webSocket.send(result.toByteString())  // Authentication
                Timer().schedule(object : TimerTask() {  // Heartbeat
                    override fun run() {
                        val heartbeatHex = "00000010001000010000000200000001"
                        webSocket.send(hexToByteArray(heartbeatHex).toByteString())
                        if (Thread.currentThread().isInterrupted) {
                            return
                        }
                    }
                }, 0L, 10000L)
            }
            override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
                if (Thread.currentThread().isInterrupted) {
                    return
                }
            }

            override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
            }

            override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
            }

            override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
                countDownLatch.countDown()
            }
        })
        countDownLatch.await()
    }
}

