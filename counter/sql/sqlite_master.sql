create table sqlite_master
(
    type     TEXT,
    name     TEXT,
    tbl_name TEXT,
    rootpage INT,
    sql      TEXT
);

INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'data', 'data', 2, 'CREATE TABLE data (id text primary key , times integer)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('index', 'sqlite_autoindex_data_1', 'data', 3, null);
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'blacked', 'blacked', 60, 'CREATE TABLE blacked(id integer primary key , base64 text, width integer, height integer)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'moebooru', 'moebooru', 61, 'CREATE TABLE moebooru(id integer primary key , base64 text, width integer, height integer)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'lisu', 'lisu', 62, 'CREATE TABLE lisu(id integer primary key , base64 text, width integer, height integer)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'lewd', 'lewd', 63, 'CREATE TABLE lewd(id integer primary key , base64 text, width integer, height integer)');
