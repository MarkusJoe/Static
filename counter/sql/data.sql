create table data
(
    id    TEXT
        primary key,
    times INTEGER
);

INSERT INTO data (id, times) VALUES ('dasd', 26);
INSERT INTO data (id, times) VALUES ('dsad', 9);
INSERT INTO data (id, times) VALUES ('dsada', 35);
INSERT INTO data (id, times) VALUES ('lewd/0', 11);
INSERT INTO data (id, times) VALUES ('asda', 126);
INSERT INTO data (id, times) VALUES ('sdas', 12);
INSERT INTO data (id, times) VALUES ('test', 11);
INSERT INTO data (id, times) VALUES ('favicon.ico', 37);
INSERT INTO data (id, times) VALUES ('api', 15);
INSERT INTO data (id, times) VALUES ('_redirect', 93);
