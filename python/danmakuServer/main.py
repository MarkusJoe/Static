import socket
import threading

from bilibili_api import live, sync


def create_sync(room: int, conn):
    room = live.LiveDanmaku(room)

    @room.on("DANMU_MSG")
    async def on_msg(event):
        data = conn.recv(1024)
        if data:
            conn.close()
        conn.send(event.encode("utf-8"))
        print(event)


class SocketServer:
    def __init__(self, port: int = 8888):
        self.__socket = socket.socket()
        self.__socket.bind(("0.0.0.0", port))
        self.__socket.listen(1024)

    def start(self):
        while True:
            conn, addr = self.__socket.accept()
            room = int(conn.recv(1024).decode("utf-8"))
            threading.Thread(target=create_sync, args=(room, conn,)).start()


if __name__ == '__main__':
    server = SocketServer()
    server.start()